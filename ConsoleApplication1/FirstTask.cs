namespace ConsoleApplication1
{
    public class FirstTask<T> : IFirstTaskIn<T>, IFirstTaskOut<T>
    {
        public readonly T Value;

        public FirstTask(T value) => Value = value;

        string IFirstTaskIn<T>.Print() => Value.ToString();

        string IFirstTaskOut<T>.Print() => Value.ToString();
    }
}